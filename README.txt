********************************************************************
                      D R U P A L    M O D U L E
********************************************************************
Name: Groupon
Author: Mohammad Anwar <er dot mohd dot anwar at gmail dot com>
Drupal: 7
********************************************************************

INTRODUCTION:
Groupon module will help admin to display groupon deals on their 
websites. This module will display groupon says, groupon deals for a
particular division and groupon deals for a particular channel. This
module will also provide three blocks each for groupon says, groupon
deals and groupon channel. This module has a administer 
configuration page by which administer can configure channel, 
division, client id, error messages and settings for groupon says page.
********************************************************************

INSTALLATION:
1. Place the entire groupon directory into your Drupal modules
    directory (normally sites/all/modules or modules).

2. Enable the exchange links module by navigating to:
    Administer > Site building > Modules
********************************************************************

CONFIGURATION:
1. Goto Configuration > Web Services > Groupon Settings
2. Enter value for all the required fields and press save.

Note: You can get your client id from http://www.groupon.com/pages/api 
********************************************************************

CONTACT:
* Mohammad Anwar (anwar_max) - http://drupal.org/user/850520
